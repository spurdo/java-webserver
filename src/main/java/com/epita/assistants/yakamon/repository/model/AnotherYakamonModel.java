package com.epita.assistants.yakamon.repository.model;

import com.epita.assistants.yakamon.arch.Model;

import java.util.UUID;

@Model
public class AnotherYakamonModel {

    final private UUID id;
    final private String name;
    final private String yakadexId;

    public AnotherYakamonModel(UUID id, String name, String yakadexId){
        this.id = id;
        this.name = name;
        this.yakadexId = yakadexId;
    }

    public UUID getId() {
        return id;
    }

    public String getName() {
        return name;
    }

    public String getYakadexId() {
        return yakadexId;
    }
}
