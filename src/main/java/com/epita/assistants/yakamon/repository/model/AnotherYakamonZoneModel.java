package com.epita.assistants.yakamon.repository.model;

import com.epita.assistants.yakamon.arch.Model;

import java.util.UUID;

@Model
public class AnotherYakamonZoneModel {

    final private UUID id;
    final private String name;
    final private String yakadexId;
    final private String zoneId;

    public AnotherYakamonZoneModel(UUID id, String name, String yakadexId, String zoneId){
        this.id = id;
        this.name = name;
        this.yakadexId = yakadexId;
        this.zoneId = zoneId;
    }

    public UUID getId() {
        return id;
    }

    public String getName() {
        return name;
    }

    public String getYakadexId() {
        return yakadexId;
    }

    public String getZoneId() {
        return zoneId;
    }
}
