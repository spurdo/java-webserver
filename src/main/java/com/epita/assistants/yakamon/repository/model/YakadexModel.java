package com.epita.assistants.yakamon.repository.model;

import com.epita.assistants.yakamon.arch.Model;

import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;

@Model
public class YakadexModel {

    private final String id;
    private final String previousEvolutionYakadexId;
    private final String nextEvolutionYakadexId;
    private List<String> moveIds = null;
    private List<String> typeIds = null;
    private List<String> zoneIds = null;

    public YakadexModel(final String id, final String previousEvolutionYakadexId, final String nextEvolutionYakadexId,
                        final String[] moveIds, final String[] typeIds, final String[] zoneIds){
        this.id = id;
        this.previousEvolutionYakadexId = previousEvolutionYakadexId;
        this.nextEvolutionYakadexId = nextEvolutionYakadexId;
        if (moveIds != null && typeIds != null && zoneIds != null) {
            this.moveIds = Arrays.asList(moveIds).stream().distinct().collect(Collectors.toList());
            this.typeIds = Arrays.asList(typeIds).stream().distinct().collect(Collectors.toList());
            this.zoneIds = Arrays.asList(zoneIds).stream().distinct().collect(Collectors.toList());
        }
    }

    public String getId() {
        return id;
    }

    public String getNextEvolutionYakadexId() {
        return nextEvolutionYakadexId;
    }

    public String getPreviousEvolutionYakadexId() {
        return previousEvolutionYakadexId;
    }


    public List<String> getMoveIds() {
        return moveIds;
    }

    public List<String> getTypeIds() {
        return typeIds;
    }

    public List<String> getZoneIds() {
        return zoneIds;
    }
}
