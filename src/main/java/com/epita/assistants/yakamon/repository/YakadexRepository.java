package com.epita.assistants.yakamon.repository;

import com.epita.assistants.yakamon.arch.Repository;
import com.epita.assistants.yakamon.repository.model.YakadexModel;
import org.jooq.DSLContext;
import org.jooq.Record6;
import org.jooq.SQLDialect;
import org.jooq.impl.DSL;

import java.sql.Connection;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

import static com.epita.assistants.ddl.Tables.*;
import static org.jooq.impl.DSL.arrayAgg;

@Repository
public class YakadexRepository {

    private DSLContext connection;

    public YakadexRepository(final Connection connection){
        this.connection = DSL.using(connection, SQLDialect.H2);
    }

    public List<YakadexModel> getYakadex(){
        List<YakadexModel> collect = new ArrayList<>();


        var res = connection
                .selectDistinct(YAKADEX.NAME, YAKADEX.NEXT_EVOLUTION, YAKADEX.PREVIOUS_EVOLUTION,
                        arrayAgg(YAKADEX_MOVES.MOVE_ID), arrayAgg(YAKADEX_TYPES.TYPE_ID), arrayAgg(YAKADEX_ZONES.ZONE_ID))
                .from(YAKADEX)
                .join(YAKADEX_MOVES).on(YAKADEX.NAME.eq(YAKADEX_MOVES.YAKADEX_ID))
                .join(YAKADEX_TYPES).on(YAKADEX.NAME.eq(YAKADEX_TYPES.YAKADEX_ID))
                .join(YAKADEX_ZONES).on(YAKADEX.NAME.eq(YAKADEX_ZONES.YAKADEX_ID))
                .groupBy(YAKADEX.NAME)
                .fetch();

        return res.stream().map(yaka -> new YakadexModel(yaka.component1(), yaka.component2(), yaka.component3(),
                yaka.component4(), yaka.component5(), yaka.component6())).collect(Collectors.toList())
                .stream().distinct().collect(Collectors.toList());

    }

    public YakadexModel getYakadexName(String yakamonName){
        var res = connection
                .selectDistinct(YAKADEX.NAME, YAKADEX.NEXT_EVOLUTION, YAKADEX.PREVIOUS_EVOLUTION,
                        arrayAgg(YAKADEX_MOVES.MOVE_ID), arrayAgg(YAKADEX_TYPES.TYPE_ID), arrayAgg(YAKADEX_ZONES.ZONE_ID))
                .from(YAKADEX)
                .join(YAKADEX_MOVES).on(YAKADEX.NAME.eq(YAKADEX_MOVES.YAKADEX_ID))
                .join(YAKADEX_TYPES).on(YAKADEX.NAME.eq(YAKADEX_TYPES.YAKADEX_ID))
                .join(YAKADEX_ZONES).on(YAKADEX.NAME.eq(YAKADEX_ZONES.YAKADEX_ID))
                .groupBy(YAKADEX.NAME)
                .fetch();

        boolean found = false;
        Record6<String, String, String, String[], String[], String[]> record = res.get(0);

        for (var rec : res){
            if (rec.get("NAME").equals(yakamonName)) {
                record = rec;
                found = true;
                break;
            }
        }

        if (!found)
            return new YakadexModel(null, null, null, null, null, null);

        return new YakadexModel(record.component1(), record.component2(), record.component3(), record.component4(),
                 record.component5(), record.component6());
    }
}
