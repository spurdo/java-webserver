package com.epita.assistants.yakamon.repository;

import com.epita.assistants.yakamon.arch.Repository;
import com.epita.assistants.yakamon.repository.model.AnotherYakamonModel;
import com.epita.assistants.yakamon.repository.model.AnotherYakamonZoneModel;
import org.jooq.DSLContext;
import org.jooq.SQLDialect;
import org.jooq.impl.DSL;

import java.sql.Connection;
import java.util.List;
import java.util.UUID;
import java.util.stream.Collectors;

import static com.epita.assistants.ddl.Tables.*;

@Repository
public class YakamonRepository {

    private DSLContext database;

    public YakamonRepository(final Connection connection){
        this.database = DSL.using(connection, SQLDialect.H2);
    }

    public List<AnotherYakamonModel> getYakamon(){
        final var res =
                database.select(YAKAMON.ID, YAKAMON.NAME, YAKAMON.YAKADEX_ID)
                        .from(YAKAMON)
                        .fetch();

        return res.stream()
                .map(record -> new AnotherYakamonModel(record.component1(), record.component2(), record.component3()))
                .collect(Collectors.toList());
    }

    public AnotherYakamonModel getYakamonId(UUID id){
        final var res =
                database.select(YAKAMON.ID, YAKAMON.NAME, YAKAMON.YAKADEX_ID)
                .from(YAKAMON)
                .where(YAKAMON.ID.eq(id))
                .fetch();

        var list = res.stream()
                .map(record -> new AnotherYakamonModel(record.component1(), record.component2(), record.component3()))
                .collect(Collectors.toList());

        if (list.size() == 0)
            return new AnotherYakamonModel(null, null, null);

        return list.get(0);
    }
}
