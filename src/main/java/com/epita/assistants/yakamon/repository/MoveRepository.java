package com.epita.assistants.yakamon.repository;

import com.epita.assistants.yakamon.arch.Repository;
import com.epita.assistants.yakamon.repository.model.MoveModel;
import org.jooq.DSLContext;
import org.jooq.SQLDialect;
import org.jooq.impl.DSL;

import java.sql.Connection;
import java.util.List;
import java.util.stream.Collectors;

@Repository
public class MoveRepository {

    final private DSLContext connection;

    public MoveRepository(final Connection connection) {
        this.connection = DSL.using(connection, SQLDialect.H2);
    }

    public List<MoveModel> listMoves(){
        return connection.selectFrom("webservice.move").stream()
                .map(move -> new MoveModel(move.get("NAME").toString()))
                .collect(Collectors.toList());
    }

    public MoveModel getMoveInfo(String moveName) {
        final var conn = connection
                .selectFrom("webservice.move")
                .where("NAME='" + moveName + "'");

        String moveNameConstructor = null;

        for (org.jooq.Record record : conn) {
            if (record.get("NAME").toString().equals(moveName)) {
                moveNameConstructor = record.get("NAME").toString();
                break;
            }
        }

        return new MoveModel(moveNameConstructor);
    }
}
