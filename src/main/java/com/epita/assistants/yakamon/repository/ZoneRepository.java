package com.epita.assistants.yakamon.repository;

import com.epita.assistants.yakamon.arch.Repository;
import com.epita.assistants.yakamon.repository.model.ZoneModel;
import org.jooq.DSLContext;
import org.jooq.SQLDialect;
import org.jooq.impl.DSL;

import java.sql.Connection;
import java.util.List;
import java.util.stream.Collectors;

@Repository
public class ZoneRepository {

    final private DSLContext connection;

    public ZoneRepository(final Connection connection){
        this.connection = DSL.using(connection, SQLDialect.H2);
    }

    public List<ZoneModel> listZones(){
        return connection.selectFrom("webservice.zone")
                .stream()
                .map(zone -> new ZoneModel(zone.get("NAME").toString()))
                .collect(Collectors.toList());
    }

    public ZoneModel getZoneInfo(String zoneName){
        final var conn = connection.selectFrom("webservice.zone")
                .where("NAME='"+zoneName+"'");

        String zoneNameConstructor = null;

        for (org.jooq.Record record : conn){
            if (record.get("NAME").toString().equals(zoneName)){
                zoneNameConstructor = record.get("NAME").toString();
                break;
            }
        }

        return new ZoneModel(zoneNameConstructor);
    }
}
