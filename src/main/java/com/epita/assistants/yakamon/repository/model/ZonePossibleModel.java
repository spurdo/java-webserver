package com.epita.assistants.yakamon.repository.model;

import com.epita.assistants.yakamon.arch.Model;

@Model
public class ZonePossibleModel {

    private String name;

    public ZonePossibleModel(final String name){
        this.name = name;
    }

    public String getName(){
        return this.name;
    }
}
