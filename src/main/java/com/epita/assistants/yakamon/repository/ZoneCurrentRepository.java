package com.epita.assistants.yakamon.repository;

import com.epita.assistants.ddl.Tables;
import com.epita.assistants.yakamon.arch.Repository;
import com.epita.assistants.yakamon.repository.model.ZoneCurrentModel;
import org.jooq.*;
import org.jooq.Record;
import org.jooq.impl.DSL;

import java.sql.Connection;
import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

@Repository
public class ZoneCurrentRepository {

    private final DSLContext database;

    public ZoneCurrentRepository(Connection connection){
        this.database = DSL.using(connection, SQLDialect.H2);
    }

    public List<ZoneCurrentModel> listYakamonInZone(String zoneName){
        var ret =
                database.select(Tables.YAKAMON.ID).from(Tables.YAKAMON)
                        .join(Tables.YAKAMON_ZONE).on(Tables.YAKAMON.ID.eq(Tables.YAKAMON_ZONE.YAKAMON_ID))
                        .join(Tables.ZONE).on(Tables.YAKAMON_ZONE.ZONE_ID.eq(Tables.ZONE.NAME))
                        .where(Tables.ZONE.NAME.eq(zoneName)).fetch();


        List<ZoneCurrentModel> modelList = new ArrayList<>();

        for (Record record : ret){
            modelList.add(new ZoneCurrentModel((UUID) record.get("ID")));
        }

        return modelList;
    }
}
