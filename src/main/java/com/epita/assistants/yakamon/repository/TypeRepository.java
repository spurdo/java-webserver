package com.epita.assistants.yakamon.repository;

import com.epita.assistants.yakamon.arch.Repository;;
import com.epita.assistants.yakamon.repository.model.TypeModel;
import org.jooq.DSLContext;
import org.jooq.SQLDialect;
import org.jooq.impl.DSL;

import java.sql.Connection;
import java.util.List;
import java.util.stream.Collectors;

@Repository
public class TypeRepository {

    private DSLContext connection;

    public TypeRepository(final Connection connection){
        this.connection = DSL.using(connection, SQLDialect.H2);
    }

    public List<TypeModel> listTypes(){
        return connection.selectFrom("webservice.type")
                .stream().map(type -> new TypeModel(type.get("NAME").toString()))
                .collect(Collectors.toList());
    }

    public TypeModel getInfo(String typeName){
        final var conn = connection
                .selectFrom("webservice.type")
                .where("NAME='"+typeName+"'");

        String typeNameConstructor = null;

        for (org.jooq.Record record : conn){
            if (record.get("NAME").toString().equals(typeName)) {
                typeNameConstructor = record.get("NAME").toString();
                break;
            }
        }
        return new TypeModel(typeNameConstructor);
    }
}
