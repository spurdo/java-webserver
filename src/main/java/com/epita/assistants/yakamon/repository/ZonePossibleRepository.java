package com.epita.assistants.yakamon.repository;

import com.epita.assistants.yakamon.arch.Repository;
import com.epita.assistants.yakamon.repository.model.ZoneCurrentModel;
import com.epita.assistants.yakamon.repository.model.ZonePossibleModel;
import org.jooq.DSLContext;
import org.jooq.SQLDialect;
import org.jooq.impl.DSL;

import java.sql.Connection;
import java.util.List;
import java.util.UUID;
import java.util.stream.Collectors;

@Repository
public class ZonePossibleRepository {

    private final DSLContext connection;

    public ZonePossibleRepository(Connection connection){
        this.connection = DSL.using(connection, SQLDialect.H2);
    }
    public List<ZonePossibleModel> listYakamonInZone(String zoneName){
        return connection.selectFrom("webservice.yakadex_zones")
                .where("ZONE_ID='"+zoneName+"'").stream()
                .map(zone -> new ZonePossibleModel(zone.get("YAKADEX_ID").toString()))
                .collect(Collectors.toList());
    }
}
