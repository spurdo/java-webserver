package com.epita.assistants.yakamon.repository.model;

import com.epita.assistants.yakamon.arch.Model;

import java.util.UUID;

@Model
public class YakamonModel {

    private final UUID uuid;

    public YakamonModel(final UUID uuid){
        this.uuid = uuid;
    }

    public UUID getUuid() {
        return uuid;
    }
}
