package com.epita.assistants.yakamon.repository;

import com.epita.assistants.ddl.Tables;
import com.epita.assistants.yakamon.arch.Repository;
import com.epita.assistants.yakamon.repository.model.TrainerModel;
import com.epita.assistants.yakamon.repository.model.YakadexModel;
import com.epita.assistants.yakamon.repository.model.YakamonModel;
import org.jooq.DSLContext;
import org.jooq.SQLDialect;
import org.jooq.impl.DSL;

import java.sql.Connection;
import java.util.List;
import java.util.UUID;
import java.util.stream.Collectors;

import static com.epita.assistants.ddl.Tables.*;

@Repository
public class TrainerRepository {

    private DSLContext database;

    public TrainerRepository(Connection connection){
        this.database = DSL.using(connection, SQLDialect.H2);
    }

    public List<TrainerModel> getTrainers(){
        final var res =
                database.select(TRAINER.UUID, TRAINER.NAME)
                        .from(TRAINER)
                        .fetch();

        return res.stream()
                .map(record -> new TrainerModel(record.component2(), record.component1()))
                .collect(Collectors.toList());
    }

    public TrainerModel getTrainer(UUID trainerId){
        final var res =
                database.select(TRAINER.UUID, TRAINER.NAME)
                .from(TRAINER)
                .where(TRAINER.UUID.eq(trainerId))
                .fetch();

        var list =
                res.stream()
                        .map(record -> new TrainerModel(record.component2(), record.component1()))
                        .collect(Collectors.toList());

        if (list.size() == 0)
            return new TrainerModel(null, null);

        return list.get(0);
    }

    public void createTrainer(TrainerModel trainerModel){
        UUID uuid = trainerModel.getUuid();
        String string = trainerModel.getName();

        database.insertInto(TRAINER)
                .values(uuid, string)
                .execute();
    }

    public void renameTrainer(UUID trainerId, String newTrainerName){
        database.update(TRAINER)
                .set(TRAINER.NAME, newTrainerName)
                .where(TRAINER.UUID.eq(trainerId))
                .execute();
    }

    public void deleteTrainer(UUID trainerId){
        //database.alterTable(TRAINER_YAKAMON)
//                .drop(TRAINER_YAKAMON.TRAINER_ID);
    }

    public List<YakamonModel> getTrainerYakamon(UUID trainerId){
        final var res = database
                .select(TRAINER_YAKAMON.YAKAMON_ID).from(TRAINER_YAKAMON)
                .where(TRAINER_YAKAMON.TRAINER_ID.eq(trainerId))
                .fetch();

        return res.stream().map(model -> new YakamonModel(model.component1())).collect(Collectors.toList());
    }
}
