package com.epita.assistants.yakamon.service;

import com.epita.assistants.yakamon.arch.Service;
import com.epita.assistants.yakamon.repository.model.MoveModel;
import com.epita.assistants.yakamon.repository.MoveRepository;
import com.epita.assistants.yakamon.service.entity.MoveEntity;

import java.util.List;
import java.util.stream.Collectors;

@Service
public class MoveService {

    final private MoveRepository moveRepository;

    public MoveService(final MoveRepository moveRepository){
        this.moveRepository = moveRepository;
    }

    public List<MoveEntity> listMoves(){
        List<MoveModel> moveModels = moveRepository.listMoves();
        return moveModels.stream().map(model -> new MoveEntity(model.getName())).collect(Collectors.toList());
    }

    public MoveEntity listMoveInfo(String moveName){
        MoveModel moveModels = moveRepository.getMoveInfo(moveName);
        return new MoveEntity(moveModels.getName());
    }
}
