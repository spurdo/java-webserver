package com.epita.assistants.yakamon.service;

import com.epita.assistants.yakamon.arch.Service;
import com.epita.assistants.yakamon.repository.model.YakadexModel;
import com.epita.assistants.yakamon.repository.YakadexRepository;
import com.epita.assistants.yakamon.service.entity.YakadexEntity;

import java.util.List;
import java.util.stream.Collectors;

@Service
public class YakadexService {

    private YakadexRepository yakadexRepository;

    public YakadexService(final YakadexRepository yakadexRepository){
        this.yakadexRepository = yakadexRepository;
    }

    public List<YakadexEntity> listYakadex(){
        List<YakadexModel> yakadexModels = yakadexRepository.getYakadex();
        return yakadexModels.stream().map(model ->
                new YakadexEntity(model.getId(),
                        model.getPreviousEvolutionYakadexId(),
                        model.getNextEvolutionYakadexId(),
                        model.getMoveIds(),
                        model.getTypeIds(),
                        model.getZoneIds())).collect(Collectors.toList());
    }

    public YakadexEntity getYakadex(String yakamonName){
        YakadexModel yakadexModel = yakadexRepository.getYakadexName(yakamonName);
        return new YakadexEntity(yakadexModel.getId(), yakadexModel.getPreviousEvolutionYakadexId(),
                yakadexModel.getNextEvolutionYakadexId(), yakadexModel.getMoveIds(), yakadexModel.getTypeIds(),
                yakadexModel.getZoneIds());
    }
}
