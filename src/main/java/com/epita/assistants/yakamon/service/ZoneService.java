package com.epita.assistants.yakamon.service;

import com.epita.assistants.yakamon.arch.Service;
import com.epita.assistants.yakamon.repository.model.ZoneModel;
import com.epita.assistants.yakamon.repository.ZoneRepository;
import com.epita.assistants.yakamon.service.entity.ZoneEntity;

import java.util.List;
import java.util.stream.Collectors;

@Service
public class ZoneService {

    final private ZoneRepository zoneRepository;

    public ZoneService(final ZoneRepository zoneRepository){
        this.zoneRepository = zoneRepository;
    }

    public List<ZoneEntity> listZones(){
        List<ZoneModel> zoneModels = zoneRepository.listZones();
        return zoneModels.stream().map(model -> new ZoneEntity(model.getName())).collect(Collectors.toList());
    }

    public ZoneEntity listZoneInfo(String zoneName){
        ZoneModel zoneModel = zoneRepository.getZoneInfo(zoneName);
        return new ZoneEntity(zoneModel.getName());
    }
}
