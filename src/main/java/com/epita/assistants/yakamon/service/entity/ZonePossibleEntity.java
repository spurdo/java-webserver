package com.epita.assistants.yakamon.service.entity;

import com.epita.assistants.yakamon.arch.Entity;

@Entity
public class ZonePossibleEntity {

    private final String name;

    public ZonePossibleEntity(final String name){
        this.name = name;
    }

    public String getName() {
        return name;
    }
}
