package com.epita.assistants.yakamon.service.entity;

import com.epita.assistants.yakamon.arch.Entity;

import java.util.UUID;

@Entity
public class YakamonEntity {

    private final UUID uuid;

    public YakamonEntity(final  UUID uuid){
        this.uuid = uuid;
    }

    public UUID getUuid(){
        return this.uuid;
    }
}
