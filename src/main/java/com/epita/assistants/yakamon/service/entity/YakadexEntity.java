package com.epita.assistants.yakamon.service.entity;

import com.epita.assistants.yakamon.arch.Entity;

import java.util.List;

@Entity
public class YakadexEntity {

    private final String id;
    private final String previousEvolutionYakadexId;
    private final String nextEvolutionYakadexId;
    private final List<String> moveIds;
    private final List<String> typeIds;
    private final List<String> zoneIds;

    public YakadexEntity(String id, String previousEvolutionYakadexId, String nextEvolutionYakadexId,
                         List<String> moveIds, List<String> typeIds, List<String> zoneIds){
        this.id = id;
        this.previousEvolutionYakadexId = previousEvolutionYakadexId;
        this.nextEvolutionYakadexId = nextEvolutionYakadexId;
        this.moveIds = moveIds;
        this.typeIds = typeIds;
        this.zoneIds = zoneIds;
    }

    public String getId() {
        return id;
    }

    public String getNextEvolutionYakadexId(){
        return this.nextEvolutionYakadexId;
    }

    public String getPreviousEvolutionYakadexId() {
        return this.previousEvolutionYakadexId;
    }


    public List<String> getMoveIds() {
        return moveIds;
    }

    public List<String> getTypeIds() {
        return typeIds;
    }

    public List<String> getZoneIds() {
        return zoneIds;
    }
}
