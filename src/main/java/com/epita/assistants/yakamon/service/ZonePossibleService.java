package com.epita.assistants.yakamon.service;

import com.epita.assistants.yakamon.arch.Service;
import com.epita.assistants.yakamon.repository.ZoneCurrentRepository;
import com.epita.assistants.yakamon.repository.ZonePossibleRepository;
import com.epita.assistants.yakamon.repository.model.ZoneCurrentModel;
import com.epita.assistants.yakamon.repository.model.ZonePossibleModel;
import com.epita.assistants.yakamon.service.entity.ZoneCurrentEntity;
import com.epita.assistants.yakamon.service.entity.ZonePossibleEntity;

import java.util.List;
import java.util.stream.Collectors;

@Service
public class ZonePossibleService {

    private ZonePossibleRepository zoneRepository;

    public ZonePossibleService(ZonePossibleRepository zoneCurrentRepository){
        this.zoneRepository = zoneCurrentRepository;
    }

    public List<ZonePossibleEntity> listPossibleYakamonInZone(String zoneName){
        List<ZonePossibleModel> zoneModels = zoneRepository.listYakamonInZone(zoneName);
        return zoneModels.stream().map(model ->
                new ZonePossibleEntity(model.getName())).collect(Collectors.toList());
    }
}
