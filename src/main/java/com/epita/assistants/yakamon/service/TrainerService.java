package com.epita.assistants.yakamon.service;


import com.epita.assistants.yakamon.arch.Service;
import com.epita.assistants.yakamon.repository.TrainerRepository;
import com.epita.assistants.yakamon.repository.model.TrainerModel;
import com.epita.assistants.yakamon.service.entity.TrainerEntity;
import com.epita.assistants.yakamon.service.entity.YakamonEntity;

import java.util.List;
import java.util.UUID;
import java.util.stream.Collectors;

@Service
public class TrainerService {

    final private TrainerRepository trainerRepository;

    public TrainerService(final TrainerRepository trainerRepository){
        this.trainerRepository = trainerRepository;
    }

    public List<TrainerEntity> getTrainers(){
        return this.trainerRepository.getTrainers()
                .stream().map(model -> new TrainerEntity(model.getUuid(), model.getName()))
                .collect(Collectors.toList());
    }

    public TrainerEntity getTrainer(UUID trainerId){
        var trainerModel = trainerRepository.getTrainer(trainerId);
        return new TrainerEntity(trainerModel.getUuid(), trainerModel.getName());
    }

    public void createTrainer(TrainerEntity trainerEntity){
        TrainerModel trainerModel = new TrainerModel(trainerEntity.getName(), trainerEntity.getUuid());
        trainerRepository.createTrainer(trainerModel);
    }

    public void renameTrainer(UUID trainerId, String newTrainerName){
        trainerRepository.renameTrainer(trainerId, newTrainerName);
    }

    public void deleteTrainer(UUID trainerId){
        trainerRepository.deleteTrainer(trainerId);
    }

    public List<YakamonEntity> getTrainerYakamon(UUID trainerId){
        return this.trainerRepository.getTrainerYakamon(trainerId)
                .stream().map(model -> new YakamonEntity(model.getUuid()))
                .collect(Collectors.toList());
    }
}
