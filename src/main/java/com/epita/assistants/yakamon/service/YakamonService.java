package com.epita.assistants.yakamon.service;

import com.epita.assistants.yakamon.arch.Service;
import com.epita.assistants.yakamon.repository.YakamonRepository;
import com.epita.assistants.yakamon.service.entity.AnotherYakamonEntity;

import java.util.List;
import java.util.UUID;
import java.util.stream.Collectors;

@Service
public class YakamonService {

    private YakamonRepository yakamonRepository;

    public YakamonService(YakamonRepository yakamonRepository){
        this.yakamonRepository = yakamonRepository;
    }

    public List<AnotherYakamonEntity> getYakamon(){
        final var models = yakamonRepository.getYakamon();
        return models
                .stream()
                .map(model -> new AnotherYakamonEntity(model.getId(), model.getName(), model.getYakadexId()))
                .collect(Collectors.toList());
    }

    public AnotherYakamonEntity getYakamonId(UUID id){
        final var entity = yakamonRepository.getYakamonId(id);

        if (entity.getId() == null)
            return new AnotherYakamonEntity(null, null, null);

        return new AnotherYakamonEntity(entity.getId(), entity.getName(), entity.getYakadexId());
    }
}
