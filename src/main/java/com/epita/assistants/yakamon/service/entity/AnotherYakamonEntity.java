package com.epita.assistants.yakamon.service.entity;

import com.epita.assistants.yakamon.arch.Entity;

import java.util.UUID;

@Entity
public class AnotherYakamonEntity {

    final private UUID id;
    final private String name;
    final private String yakadexId;

    public AnotherYakamonEntity(final UUID id, final String name, final String yakadexId){
        this.id = id;
        this.name = name;
        this.yakadexId = yakadexId;
    }

    public UUID getId() {
        return id;
    }

    public String getName() {
        return name;
    }

    public String getYakadexId() {
        return yakadexId;
    }
}
