package com.epita.assistants.yakamon.service;

import com.epita.assistants.yakamon.arch.Service;
import com.epita.assistants.yakamon.repository.model.TypeModel;
import com.epita.assistants.yakamon.repository.TypeRepository;
import com.epita.assistants.yakamon.service.entity.TypeEntity;

import java.util.List;
import java.util.stream.Collectors;

@Service
public class TypeService {

    private TypeRepository typeRepository;

    public TypeService(final TypeRepository typeRepository){
        this.typeRepository = typeRepository;
    }

    public List<TypeEntity> listTypes(){
        List<TypeModel> typeModels = typeRepository.listTypes();
        return typeModels.stream().map(model -> new TypeEntity(model.getName())).collect(Collectors.toList());
    }

    public TypeEntity getInfo(String typeName){
        TypeModel typeModel = typeRepository.getInfo(typeName);
        return new TypeEntity(typeModel.getName());
    }
}
