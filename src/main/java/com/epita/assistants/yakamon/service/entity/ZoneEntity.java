package com.epita.assistants.yakamon.service.entity;

import com.epita.assistants.yakamon.arch.Entity;

@Entity
public class ZoneEntity {

    private final String name;

    public ZoneEntity(final String name){
        this.name = name;
    }

    public String getZoneName(){
        return this.name;
    }
}
