package com.epita.assistants.yakamon.service;

import com.epita.assistants.yakamon.arch.Service;
import com.epita.assistants.yakamon.repository.ZoneCurrentRepository;
import com.epita.assistants.yakamon.service.entity.ZoneCurrentEntity;

import java.util.List;
import java.util.stream.Collectors;

@Service
public class ZoneCurrentService {

    private ZoneCurrentRepository zoneRepository;

    public ZoneCurrentService(ZoneCurrentRepository zoneRepository){
        this.zoneRepository = zoneRepository;
    }

    public List<ZoneCurrentEntity> listCurrentYakamonInZone(String zoneName){
        var zoneModels = zoneRepository.listYakamonInZone(zoneName);

        return zoneModels.stream().map(model ->
                new ZoneCurrentEntity(model.getUuid())).collect(Collectors.toList());
    }
}
