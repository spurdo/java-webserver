package com.epita.assistants.yakamon.error;

import java.util.Map;
import java.util.stream.Collectors;
import java.util.stream.Stream;

public enum ErrorEnum {
        OBJECT_NOT_FOUND,
        DELETION_FAILED,
        UPDATE_FAILED,
        CREATION_FAILED,
        BAD_PARAMETER,
        BAD_BODY;

        static public Map<ErrorEnum, String> errorMessage = Stream.of(new Object[][]{
                {OBJECT_NOT_FOUND, "Object not found"},
                {DELETION_FAILED, "Deletion failed"},
                {UPDATE_FAILED, "Update failed"},
                {CREATION_FAILED, "Object creation failed"},
                {BAD_PARAMETER, "Bad parameter"},
                {BAD_BODY, "Bad body"},
        }).collect(Collectors.toMap(data -> (ErrorEnum) data[0], data -> (String) data[1]));
}