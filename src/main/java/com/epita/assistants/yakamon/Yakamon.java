package com.epita.assistants.yakamon;

import com.epita.assistants.yakamon.controller.*;
import com.epita.assistants.yakamon.repository.*;
import com.epita.assistants.yakamon.service.*;

import org.h2.jdbcx.JdbcDataSource;
import spark.Spark;

import java.sql.Connection;
import java.sql.SQLException;
import java.util.Optional;

/**
 * Entry point for Yakamon.
 *
 */
public final class Yakamon {

    /**
     * Setup database connection
     *
     * @return Optional with the Connection if succeed
     */
    private static Optional<Connection> setupConnection() {
        final JdbcDataSource dataSource = new JdbcDataSource();
        dataSource.setURL("jdbc:h2:./webservice");
        dataSource.setUser("sa");
        dataSource.setPassword("");
        try {
            return Optional.of(dataSource.getConnection());
        } catch (SQLException sqlException) {
            return Optional.empty();
        }
    }

    /**
     * CONTROLLER Set up
     */
    private static ZonePossibleController setUpZonePossibleController(Connection connection){
        ZonePossibleRepository zonePossibleRepository = new ZonePossibleRepository(connection);
        ZonePossibleService zonePossibleService = new ZonePossibleService(zonePossibleRepository);
        return new ZonePossibleController(zonePossibleService);
    }

    private static ZoneCurrentController setUpZoneCurrentController(Connection connection){
        ZoneCurrentRepository zoneCurrentRepository = new ZoneCurrentRepository(connection);
        ZoneCurrentService zoneCurrentService = new ZoneCurrentService(zoneCurrentRepository);
        return new ZoneCurrentController(zoneCurrentService);
    }

    private static TypeController setUpTypeController(Connection connection){
        TypeRepository typeRepository = new TypeRepository(connection);
        TypeService typeService = new TypeService(typeRepository);
        return new TypeController(typeService);
    }

    private static ZoneController setUpZoneController(Connection connection){
        ZoneRepository ZoneRepository = new ZoneRepository(connection);
        ZoneService zoneService = new ZoneService(ZoneRepository);
        return new ZoneController(zoneService);
    }

    private static MoveController setUpMoveController(Connection connection){
        MoveRepository moveRepository = new MoveRepository(connection);
        MoveService moveService = new MoveService(moveRepository);
        return new MoveController(moveService);
    }

    private static YakadexController setUpYakadexController(Connection connection){
        YakadexRepository yakadexRepository = new YakadexRepository(connection);
        YakadexService yakadexService = new YakadexService(yakadexRepository);
        return new YakadexController(yakadexService);
    }

    private static TrainerController setUpTrainerController(Connection connection){
        TrainerRepository trainerRepository = new TrainerRepository(connection);
        TrainerService trainerService = new TrainerService(trainerRepository);
        return new TrainerController(trainerService);
    }

    private static YakamonController setUpYakamonController(Connection connection){
        YakamonRepository yakamonRepository = new YakamonRepository(connection);
        YakamonService yakamonService = new YakamonService(yakamonRepository);
        return new YakamonController(yakamonService);
    }

    /**
     * ENDPOINT Registration
     */
    private static void registerTypeEndPoints(TypeController typeController){
        Spark.get("/type/", typeController::getType);
        Spark.get("/type/:name", typeController::getTypeInfo);
    }

    private static void registerZoneEndPoints(ZoneController zoneController){
        Spark.get("/zone/", zoneController::getZones);
        Spark.get("/zone/:name", zoneController::getZoneInfo);
            }

    private static void registerZoneCurrentEndPoints(ZoneCurrentController zoneCurrentController){
        Spark.get("/zone/:name/currentYakamons", zoneCurrentController::getCurrentYakamonsInZone);
    }

    private static void registerZonePossibleEndPoints(ZonePossibleController zonePossibleController){
        Spark.get("/zone/:name/possibleYakamons", zonePossibleController::getPossibleYakamonsInZone);
    }

    private static void registerMoveEndPoints(MoveController moveController){
        Spark.get("/move/", moveController::getMoves);
        Spark.get("/move/:name", moveController::getMoveInfo);
    }

    private static void registerYakadexEndPoints(YakadexController yakadexController){
        Spark.get("/yakadex/", yakadexController::getYakadex);
        Spark.get("/yakadex/:name", yakadexController::getYakadexName);
    }

    private static void registerTrainerEndPoints(TrainerController trainerController){
        Spark.get("/trainer/", trainerController::getTrainers);
        Spark.put("/trainer/", trainerController::createTrainer);
        Spark.get("/trainer/:uuid", trainerController::getTrainer);
        Spark.patch("/trainer/:uuid", trainerController::renameTrainer);
        Spark.get("/trainer/:uuid/yakamons", trainerController::getTrainerYakamon);
        //Spark.delete("/trainer/:uuid", trainerController::deleteTrainer);
        //Spark.post("/trainer/:uuid/yakamons/:yakamon", trainerController::addYakamon);
    }

    private static void registerYakamonEndPoints(YakamonController yakamonController){
        Spark.get("/yakamon/", yakamonController::getYakamon);
        Spark.get("/yakamon/:id", yakamonController::getYakamonId);
    }

    /**
     * MAIN Function (La Emu.)
     */
    public static void start(final int port) {
        // 0 - Set up connection
        Optional<Connection> optionalConnection = setupConnection();
        if (optionalConnection.isEmpty())
            throw new RuntimeException("Could not setup connection.");

        Connection connection = optionalConnection.get();

        // 1 - Instantiate your Controllers
        TypeController typeController = setUpTypeController(connection);
        ZoneController zoneController = setUpZoneController(connection);
        ZonePossibleController zonePossibleController = setUpZonePossibleController(connection);
        ZoneCurrentController zoneCurrentController = setUpZoneCurrentController(connection);
        MoveController moveController = setUpMoveController(connection);
        YakadexController yakadexController = setUpYakadexController(connection);
        TrainerController trainerController = setUpTrainerController(connection);
        YakamonController yakamonController = setUpYakamonController(connection);

        // 2 - Set the port of spark
        Spark.port(port);

        // 3 - Register your endpoints
        Spark.before("/*", (a, b) -> b.type("application/json"));

        registerTypeEndPoints(typeController);
        registerZoneEndPoints(zoneController);
        registerZonePossibleEndPoints(zonePossibleController);
        registerZoneCurrentEndPoints(zoneCurrentController);
        registerMoveEndPoints(moveController);
        registerYakadexEndPoints(yakadexController);
        registerTrainerEndPoints(trainerController);
        registerYakamonEndPoints(yakamonController);
    }

    /**
     * Main method.
     *
     * @param args Command line arguments.
     */
    public static void main(final String... args) {
        start(4567);
    }
}
