package com.epita.assistants.yakamon.controller.request;

import com.epita.assistants.yakamon.arch.DtoRequest;

import java.util.UUID;

@DtoRequest
public class TrainerRequest {

    public UUID uuid;
    public String name;

    public TrainerRequest(){

    }
}
