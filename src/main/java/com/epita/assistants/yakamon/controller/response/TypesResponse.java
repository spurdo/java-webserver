package com.epita.assistants.yakamon.controller.response;

import com.epita.assistants.yakamon.arch.DtoResponse;

import java.util.List;

@DtoResponse
public class TypesResponse extends Response {

    private List<String> types;

    public TypesResponse(List<String> types){
        super();
        this.types = types;
    }

    public List<String> getTypes(){
        return this.types;
    }

    public void setTypes(List<String> types){
        this.types = types;
    }

 }
