package com.epita.assistants.yakamon.controller.response;

import com.epita.assistants.yakamon.arch.DtoResponse;

@DtoResponse
public class ZoneResponse extends Response {

    private String name;

    public ZoneResponse(String name) {
        super();
        this.name = name;
    }

    public String getName() {
        return name;
    }

    public void setZone(String zoneName) {
        this.name = zoneName;
    }
}