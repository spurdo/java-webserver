package com.epita.assistants.yakamon.controller.response;

import com.epita.assistants.yakamon.arch.DtoResponse;
import com.epita.assistants.yakamon.service.entity.AnotherYakamonEntity;

import java.util.List;

@DtoResponse
public class AnotherYakamonResponse extends Response{

    private final List<AnotherYakamonEntity> entityList;

    public AnotherYakamonResponse(List<AnotherYakamonEntity> entityList){
        this.entityList = entityList;
    }

    public List<AnotherYakamonEntity> getEntityList() {
        return entityList;
    }
}
