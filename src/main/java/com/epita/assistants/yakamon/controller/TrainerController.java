package com.epita.assistants.yakamon.controller;

import com.epita.assistants.yakamon.arch.Controller;
import com.epita.assistants.yakamon.controller.request.TrainerRenameRequest;
import com.epita.assistants.yakamon.controller.request.TrainerRequest;
import com.epita.assistants.yakamon.controller.response.TrainerNameResponse;
import com.epita.assistants.yakamon.controller.response.TrainerResponse;
import com.epita.assistants.yakamon.controller.response.TrainersResponse;
import com.epita.assistants.yakamon.controller.response.YakamonResponse;
import com.epita.assistants.yakamon.error.ErrorEnum;
import com.epita.assistants.yakamon.service.entity.TrainerEntity;
import com.epita.assistants.yakamon.service.*;
import com.epita.assistants.yakamon.service.entity.YakamonEntity;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import spark.Request;
import spark.Response;

import java.util.List;
import java.util.UUID;
import java.util.stream.Collectors;

@Controller
public class TrainerController {

    private TrainerService trainerService;

    public TrainerController(final TrainerService trainerService){
        this.trainerService = trainerService;
    }

    public String getTrainers(Request request, Response response) throws JsonProcessingException {
        List<TrainerEntity> entityList = trainerService.getTrainers();
        ObjectMapper objectMapper = new ObjectMapper();
        TrainersResponse trainersResponse = new TrainersResponse(entityList);

        return objectMapper.writeValueAsString(trainersResponse);
    }

    public String getTrainer(Request request, Response response) throws JsonProcessingException{

        UUID trainerId = UUID.fromString(request.params(":uuid"));
        TrainerEntity entity = trainerService.getTrainer(trainerId);
        ObjectMapper objectMapper = new ObjectMapper();
        TrainerResponse trainerResponse = new TrainerResponse(entity.getUuid(), entity.getName());

        if (trainerResponse.getName() == null || trainerResponse.getUuid() == null)
            trainerResponse.setError(ErrorEnum.OBJECT_NOT_FOUND);

        return objectMapper.writeValueAsString(trainerResponse);
    }

    public String createTrainer(Request request, Response response) throws JsonProcessingException{
        ObjectMapper objectMapper = new ObjectMapper();

        try {
            TrainerRequest trainerRequest = objectMapper.readValue(request.body(), TrainerRequest.class);
            TrainerEntity entity = new TrainerEntity(trainerRequest.uuid, trainerRequest.name);
            trainerService.createTrainer(entity);
            TrainerResponse trainerResponse = new TrainerResponse(trainerRequest.uuid, trainerRequest.name);

            return objectMapper.writeValueAsString(trainerResponse);
        } catch (JsonMappingException e){
            e.printStackTrace();
        }

        return null;
    }

    public String renameTrainer(Request request, Response response) throws JsonProcessingException {
        ObjectMapper objectMapper = new ObjectMapper();
        try {
            TrainerRenameRequest trainerRenameRequest = objectMapper.readValue(request.body(), TrainerRenameRequest.class);

            UUID trainerId = UUID.fromString(request.params(":uuid"));

            trainerService.renameTrainer(trainerId, trainerRenameRequest.name);
            return objectMapper.writeValueAsString(new TrainerResponse(trainerId, trainerRenameRequest.name));
        } catch (JsonMappingException e) {
            e.printStackTrace();
        }
        return null;
    }

    public String getTrainerYakamon(Request request, Response response) throws JsonProcessingException{
        ObjectMapper objectMapper = new ObjectMapper();
        UUID trainerId = UUID.fromString(request.params(":uuid"));
        List<YakamonEntity> entityList = trainerService.getTrainerYakamon(trainerId);
        YakamonResponse yakamonResponse =
                new YakamonResponse(entityList.stream().map(YakamonEntity::getUuid).collect(Collectors.toList()));

        if (yakamonResponse.getYakamonList().size() == 0)
            yakamonResponse.setError(ErrorEnum.OBJECT_NOT_FOUND);

        return objectMapper.writeValueAsString(yakamonResponse);
    }

    public String deleteTrainer(Request request, Response response) throws JsonProcessingException{
        ObjectMapper objectMapper = new ObjectMapper();
        UUID trainerId = UUID.fromString(request.params(":uuid"));
        trainerService.deleteTrainer(trainerId);

        return objectMapper.writeValueAsString(new TrainerNameResponse());
    }

    public String addYakamon(Request request, Response response) throws JsonProcessingException{
        return null;
    }
}
