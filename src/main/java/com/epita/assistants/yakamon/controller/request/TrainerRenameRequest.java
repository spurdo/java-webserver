package com.epita.assistants.yakamon.controller.request;

import com.epita.assistants.yakamon.arch.DtoRequest;

@DtoRequest
public class TrainerRenameRequest {

    public String name;

    public TrainerRenameRequest(){

    }
}
