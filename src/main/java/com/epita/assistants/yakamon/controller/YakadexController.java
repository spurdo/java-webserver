package com.epita.assistants.yakamon.controller;

import com.epita.assistants.yakamon.arch.Controller;
import com.epita.assistants.yakamon.controller.response.YakadexResponse;
import com.epita.assistants.yakamon.controller.response.YakadexesResponse;
import com.epita.assistants.yakamon.error.ErrorEnum;
import com.epita.assistants.yakamon.service.entity.YakadexEntity;
import com.epita.assistants.yakamon.service.YakadexService;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import spark.Request;
import spark.Response;

import java.util.List;

@Controller
public class YakadexController {

    private YakadexService yakadexService;

    public YakadexController(final YakadexService yakadexService){
        this.yakadexService = yakadexService;
    }

    public String getYakadex(Request request, Response response) throws JsonProcessingException{
        final List<YakadexEntity> yakadexEntries = yakadexService.listYakadex();
        ObjectMapper objectMapper = new ObjectMapper();
        YakadexesResponse yakadexesResponse =
                new YakadexesResponse(yakadexEntries);

        return objectMapper.writeValueAsString(yakadexesResponse);
    }

    public String getYakadexName(Request request, Response response) throws JsonProcessingException{
        final YakadexEntity yakadexEntity = yakadexService.getYakadex(request.params(":name"));
        ObjectMapper objectMapper = new ObjectMapper();
        YakadexResponse yakadexResponse = new YakadexResponse(yakadexEntity.getId(),
                yakadexEntity.getPreviousEvolutionYakadexId(), yakadexEntity.getNextEvolutionYakadexId(),
                yakadexEntity.getMoveIds(), yakadexEntity.getTypeIds(), yakadexEntity.getZoneIds());

        if (yakadexResponse.getId() == null)
            yakadexResponse.setError(ErrorEnum.OBJECT_NOT_FOUND);

        return objectMapper.writeValueAsString(yakadexResponse);
    }
}
