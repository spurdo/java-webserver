package com.epita.assistants.yakamon.controller.response;

import com.epita.assistants.yakamon.arch.DtoResponse;

@DtoResponse
public class TypeResponse extends Response{

    private String name;

    public TypeResponse(String name){
        super();
        this.name = name;
    }

    public String getName(){
        return this.name;
    }

    public void setTypesList(String typesName){
        this.name = typesName;
    }
  }
