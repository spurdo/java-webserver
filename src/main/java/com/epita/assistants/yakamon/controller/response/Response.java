package com.epita.assistants.yakamon.controller.response;

import com.epita.assistants.yakamon.arch.DtoResponse;
import com.epita.assistants.yakamon.error.ErrorEnum;

@DtoResponse
public abstract class Response {

    private String status = "SUCCESS";
    private String errorCode = null;
    private String errorMessage = null;

    public Response(){}

    public void setError(ErrorEnum errorEnum){
        this.status = "FAILURE";
        this.errorCode = errorEnum.toString();
        this.errorMessage = ErrorEnum.errorMessage.get(errorEnum);
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getErrorCode() {
        return errorCode;
    }

    public void setErrorCode(String errorCode) {
        this.errorCode = errorCode;
    }

    public String getErrorMessage() {
        return errorMessage;
    }

    public void setErrorMessage(String errorMessage) {
        this.errorMessage = errorMessage;
    }
}
