package com.epita.assistants.yakamon.controller;

import com.epita.assistants.yakamon.arch.Controller;

import com.epita.assistants.yakamon.controller.response.TypeResponse;
import com.epita.assistants.yakamon.controller.response.TypesResponse;
import com.epita.assistants.yakamon.error.ErrorEnum;
import com.epita.assistants.yakamon.service.entity.TypeEntity;
import com.epita.assistants.yakamon.service.TypeService;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import spark.Request;
import spark.Response;

import java.util.List;
import java.util.stream.Collectors;

@Controller
public class TypeController {

    private final TypeService typeService;

    public TypeController(final TypeService typeService){
        this.typeService = typeService;
    }

    public String getType(Request request, Response response) throws JsonProcessingException {
        final List<TypeEntity> entityList = typeService.listTypes();
        ObjectMapper objectMapper = new ObjectMapper();
        TypesResponse typesResponse =
                new TypesResponse(entityList.stream().map(TypeEntity::getName).collect(Collectors.toList()));

        return objectMapper.writeValueAsString(typesResponse);
    }

    public String getTypeInfo(Request request, Response response) throws JsonProcessingException {
        final TypeEntity typeEntity = typeService.getInfo(request.params(":name"));
        ObjectMapper objectMapper = new ObjectMapper();
        TypeResponse typeResponse = new TypeResponse(typeEntity.getName());

        if (typeResponse.getName() == null)
            typeResponse.setError(ErrorEnum.OBJECT_NOT_FOUND);

        return objectMapper.writeValueAsString(typeResponse);
    }
}
