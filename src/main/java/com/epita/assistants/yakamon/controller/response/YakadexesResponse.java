package com.epita.assistants.yakamon.controller.response;

import com.epita.assistants.yakamon.arch.DtoResponse;
import com.epita.assistants.yakamon.service.entity.YakadexEntity;

import java.util.List;

@DtoResponse
public class YakadexesResponse extends Response{

    private final List<YakadexEntity> yakadexEntries;

    public YakadexesResponse(List<YakadexEntity> entityList){
        super();
        this.yakadexEntries = entityList;
    }

    public List<YakadexEntity> getEntityList() {
        return yakadexEntries;
    }
}
