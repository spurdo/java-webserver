package com.epita.assistants.yakamon.controller.response;

import com.epita.assistants.yakamon.arch.DtoResponse;

import java.util.UUID;

@DtoResponse
public class TrainerResponse extends Response{

    final private UUID uuid;
    final private String name;

    public TrainerResponse(final UUID uuid, final String name){
        super();
        this.name = name;
        this.uuid = uuid;
    }

    public String getName() {
        return name;
    }

    public UUID getUuid() {
        return uuid;
    }
}
