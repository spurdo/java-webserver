package com.epita.assistants.yakamon.controller.response;

import com.epita.assistants.yakamon.arch.DtoResponse;

import java.util.List;

@DtoResponse
public class ZonePossibleResponse extends Response{

    private List<String> yakadexIds;

    public ZonePossibleResponse(final List<String> yakadexIds){
        super();
        this.yakadexIds = yakadexIds;
    }

    public List<String> getYakadexIds() {
        return yakadexIds;
    }

    public void setYakadexIds(List<String> yakadexIds) {
        this.yakadexIds = yakadexIds;
    }
}
