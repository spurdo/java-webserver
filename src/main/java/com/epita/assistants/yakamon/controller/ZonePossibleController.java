package com.epita.assistants.yakamon.controller;

import com.epita.assistants.yakamon.arch.Controller;
import com.epita.assistants.yakamon.controller.response.ZonePossibleResponse;
import com.epita.assistants.yakamon.service.ZonePossibleService;
import com.epita.assistants.yakamon.service.entity.ZoneCurrentEntity;
import com.epita.assistants.yakamon.service.entity.ZonePossibleEntity;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import spark.Request;
import spark.Response;

import java.util.List;
import java.util.stream.Collectors;

@Controller
public class ZonePossibleController {

    private ZonePossibleService zonePossibleService;

    public ZonePossibleController(ZonePossibleService zonePossibleService){
        this.zonePossibleService = zonePossibleService;
    }

    public String getPossibleYakamonsInZone(Request request, Response response) throws JsonProcessingException {
        final List<ZonePossibleEntity> zoneEntityList = zonePossibleService.listPossibleYakamonInZone(request.params(":name"));
        ObjectMapper objectMapper = new ObjectMapper();

        ZonePossibleResponse zonesResponse =
                new ZonePossibleResponse(zoneEntityList.stream().map(ZonePossibleEntity::getName).collect(Collectors.toList()));

        return objectMapper.writeValueAsString(zonesResponse);
    }
}
