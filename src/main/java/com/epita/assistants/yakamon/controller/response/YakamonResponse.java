package com.epita.assistants.yakamon.controller.response;

import com.epita.assistants.yakamon.arch.DtoResponse;

import java.util.List;
import java.util.UUID;

@DtoResponse
public class YakamonResponse extends Response{

    private final List<UUID> yakamonList;

    public YakamonResponse(List<UUID> yakamonList){
        this.yakamonList = yakamonList;
    }

    public List<UUID> getYakamonList() {
        return yakamonList;
    }
}
