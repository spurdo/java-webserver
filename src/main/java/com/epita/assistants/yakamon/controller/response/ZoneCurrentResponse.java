package com.epita.assistants.yakamon.controller.response;

import com.epita.assistants.yakamon.arch.DtoResponse;

import java.util.List;
import java.util.UUID;

@DtoResponse
public class ZoneCurrentResponse extends Response{

    private List<UUID> yakamonIds;

    public ZoneCurrentResponse(final List<UUID> yakamonIds){
        super();
        this.yakamonIds = yakamonIds;
    }

    public List<UUID> getYakamonIds() {
        return yakamonIds;
    }

    public void setYakamonIds(List<UUID> yakamonIds) {
        this.yakamonIds = yakamonIds;
    }
}
