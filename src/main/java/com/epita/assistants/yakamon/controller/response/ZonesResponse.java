package com.epita.assistants.yakamon.controller.response;

import com.epita.assistants.yakamon.arch.DtoResponse;

import java.util.List;

@DtoResponse
public class ZonesResponse extends Response {

    private List<String> zones;

    public ZonesResponse(List<String> zones){
        super();
        this.zones = zones;
    }

    public List<String> getZones() {
        return zones;
    }

    public void setZones(List<String> zones) {
        this.zones = zones;
    }
}
