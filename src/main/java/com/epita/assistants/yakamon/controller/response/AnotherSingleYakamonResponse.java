package com.epita.assistants.yakamon.controller.response;

import com.epita.assistants.yakamon.arch.DtoResponse;

import java.util.UUID;

@DtoResponse
public class AnotherSingleYakamonResponse extends Response{

    final private UUID id;
    final private String name;
    final private String yakadexId;

    public AnotherSingleYakamonResponse(UUID id, String name, String yakadexId){
        this.id = id;
        this.name = name;
        this.yakadexId = yakadexId;
    }

    public UUID getId() {
        return id;
    }

    public String getName() {
        return name;
    }

    public String getYakadexId() {
        return yakadexId;
    }
}
