package com.epita.assistants.yakamon.controller;

import com.epita.assistants.yakamon.arch.Controller;
import com.epita.assistants.yakamon.controller.response.MoveResponse;
import com.epita.assistants.yakamon.controller.response.MovesResponse;
import com.epita.assistants.yakamon.error.ErrorEnum;
import com.epita.assistants.yakamon.service.entity.MoveEntity;
import com.epita.assistants.yakamon.service.MoveService;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import spark.Request;
import spark.Response;

import java.util.List;
import java.util.stream.Collectors;

@Controller
public class MoveController {

    final private MoveService moveService;

    public MoveController(final MoveService moveService){
        this.moveService = moveService;
    }

    public String getMoves(Request request, Response response) throws JsonProcessingException {
        final List<MoveEntity> entityList = moveService.listMoves();
        ObjectMapper objectMapper = new ObjectMapper();
        MovesResponse movesResponse =
                new MovesResponse(entityList.stream().map(MoveEntity::getName).collect(Collectors.toList()));

        return objectMapper.writeValueAsString(movesResponse);
    }

    public String getMoveInfo(Request request, Response response) throws JsonProcessingException{
        final MoveEntity entity = moveService.listMoveInfo(request.params(":name"));
        ObjectMapper objectMapper = new ObjectMapper();
        MoveResponse moveResponse = new MoveResponse(entity.getName());

        if (moveResponse.getName() == null)
            moveResponse.setError(ErrorEnum.OBJECT_NOT_FOUND);

        return objectMapper.writeValueAsString(moveResponse);
    }
}
