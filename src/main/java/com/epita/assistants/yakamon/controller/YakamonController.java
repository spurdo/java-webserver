package com.epita.assistants.yakamon.controller;

import com.epita.assistants.yakamon.arch.Controller;
import com.epita.assistants.yakamon.controller.response.AnotherSingleYakamonResponse;
import com.epita.assistants.yakamon.controller.response.AnotherYakamonResponse;
import com.epita.assistants.yakamon.error.ErrorEnum;
import com.epita.assistants.yakamon.service.YakamonService;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import spark.Request;
import spark.Response;

import java.util.UUID;

@Controller
public class YakamonController {

    private YakamonService yakamonService;

    public YakamonController(YakamonService yakamonService){
        this.yakamonService = yakamonService;
    }

    public String getYakamon(Request request, Response response) throws JsonProcessingException {
        final var list = yakamonService.getYakamon();

        ObjectMapper objectMapper = new ObjectMapper();
        AnotherYakamonResponse anotherYakamonResponse = new AnotherYakamonResponse(list);

        return objectMapper.writeValueAsString(anotherYakamonResponse);
    }

    public String getYakamonId(Request request, Response response) throws JsonProcessingException{
        UUID id = UUID.fromString(request.params(":id"));
        final var yakamon = yakamonService.getYakamonId(id);
        AnotherSingleYakamonResponse anotherSingleYakamonResponse =
                new AnotherSingleYakamonResponse(yakamon.getId(), yakamon.getName(), yakamon.getYakadexId());

        if (yakamon.getId() == null)
            anotherSingleYakamonResponse.setError(ErrorEnum.OBJECT_NOT_FOUND);

        ObjectMapper objectMapper = new ObjectMapper();


        return objectMapper.writeValueAsString(anotherSingleYakamonResponse);
    }
}
