package com.epita.assistants.yakamon.controller;

import com.epita.assistants.yakamon.arch.Controller;
import com.epita.assistants.yakamon.controller.response.ZoneCurrentResponse;
import com.epita.assistants.yakamon.service.ZoneCurrentService;
import com.epita.assistants.yakamon.service.entity.ZoneCurrentEntity;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import spark.Request;
import spark.Response;

import java.util.List;
import java.util.stream.Collectors;

@Controller
public class ZoneCurrentController {

    private ZoneCurrentService zoneCurrentService;

    public ZoneCurrentController(ZoneCurrentService zoneCurrentService){
        this.zoneCurrentService = zoneCurrentService;
    }

    public String getCurrentYakamonsInZone(Request request, Response response) throws JsonProcessingException {
        final List<ZoneCurrentEntity> zoneEntityList = zoneCurrentService.listCurrentYakamonInZone(request.params(":name"));
        ObjectMapper objectMapper = new ObjectMapper();

        ZoneCurrentResponse zonesResponse =
                new ZoneCurrentResponse(zoneEntityList.stream().map(ZoneCurrentEntity::getUuid).collect(Collectors.toList()));

        return objectMapper.writeValueAsString(zonesResponse);
    }
}
