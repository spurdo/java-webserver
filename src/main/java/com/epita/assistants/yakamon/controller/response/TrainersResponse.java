package com.epita.assistants.yakamon.controller.response;

import com.epita.assistants.yakamon.arch.DtoResponse;
import com.epita.assistants.yakamon.service.entity.TrainerEntity;

import java.util.List;

@DtoResponse
public class TrainersResponse extends Response{

    List<TrainerEntity> trainers;
    public TrainersResponse(List<TrainerEntity> trainers){
        super();
        this.trainers = trainers;
    }

    public List<TrainerEntity> getTrainers(){
        return this.trainers;
    }
}
