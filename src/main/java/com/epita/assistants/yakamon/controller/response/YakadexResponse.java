package com.epita.assistants.yakamon.controller.response;

import com.epita.assistants.yakamon.arch.DtoResponse;
import com.epita.assistants.yakamon.service.entity.YakadexEntity;

import java.util.ArrayList;
import java.util.List;

@DtoResponse
public class YakadexResponse extends Response{

    private final String id;
    private final String previousEvolutionYakadexId;
    private final String nextEvolutionYakadexId;
    private final List<String> moveIds;
    private final List<String> typeIds;
    private final List<String> zoneIds;

    public YakadexResponse(String id, String previousEvolutionYakadexId, String nextEvolutionYakadexId,
                           List<String> moveIds, List<String> typeIds, List<String> zoneIds){
        super();
        this.id = id;
        this.previousEvolutionYakadexId = previousEvolutionYakadexId;
        this.nextEvolutionYakadexId = nextEvolutionYakadexId;
        this.moveIds = moveIds;
        this.typeIds = typeIds;
        this.zoneIds = zoneIds;
    }

    public String getId() {
        return id;
    }

    public String getPreviousEvolutionYakadexId() {
        return previousEvolutionYakadexId;
    }

    public String getNextEvolutionYakadexId() {
        return nextEvolutionYakadexId;
    }

    public List<String> getMoveIds() {
        return moveIds;
    }

    public List<String> getTypeIds() {
        return typeIds;
    }

    public List<String> getZoneIds() {
        return zoneIds;
    }
}
