package com.epita.assistants.yakamon.controller;

import com.epita.assistants.yakamon.arch.Controller;
import com.epita.assistants.yakamon.controller.response.ZoneResponse;
import com.epita.assistants.yakamon.controller.response.ZonesResponse;
import com.epita.assistants.yakamon.error.ErrorEnum;
import com.epita.assistants.yakamon.service.entity.ZoneEntity;
import com.epita.assistants.yakamon.service.ZoneService;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import spark.Request;
import spark.Response;

import java.util.List;
import java.util.stream.Collectors;

@Controller
public class ZoneController {

    final private ZoneService zoneService;

    public ZoneController(final ZoneService zoneService){
        this.zoneService = zoneService;
    }

    public String getZones(Request request, Response response) throws JsonProcessingException {
        final List<ZoneEntity> zoneEntityList = zoneService.listZones();
        ObjectMapper objectMapper = new ObjectMapper();

        ZonesResponse zonesResponse =
                new ZonesResponse(zoneEntityList.stream().map(ZoneEntity::getZoneName).collect(Collectors.toList()));

        return objectMapper.writeValueAsString(zonesResponse);
    }

    public String getZoneInfo(Request request, Response response) throws JsonProcessingException{
        final ZoneEntity zoneEntity = zoneService.listZoneInfo(request.params(":name"));
        ObjectMapper objectMapper = new ObjectMapper();

        ZoneResponse zoneResponse = new ZoneResponse(zoneEntity.getZoneName());

        if (zoneResponse.getName() == null)
            zoneResponse.setError(ErrorEnum.OBJECT_NOT_FOUND);

        return objectMapper.writeValueAsString(zoneResponse);
    }
}
